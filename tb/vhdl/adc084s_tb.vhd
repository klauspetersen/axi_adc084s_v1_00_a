library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use ieee.math_real.all;

library axi_adc084s_v1_00_a;
use axi_adc084s_v1_00_a.defs.all;
use axi_adc084s_v1_00_a.all;

entity adc084s_tb is
end adc084s_tb;

architecture behavior of adc084s_tb is

	--Inputs
	signal clk      : std_logic := '0';
	signal rst      : std_logic := '0';
	signal clk_div  : std_logic_vector(15 downto 0);
	signal en       : std_logic;
	signal done     : std_logic;
	signal cs_n     : std_logic;
	signal sclk     : std_logic;
	signal mosi     : std_logic;
	signal channel0 : std_logic_vector(7 downto 0);
	signal channel1 : std_logic_vector(7 downto 0);
	signal channel2 : std_logic_vector(7 downto 0);
	signal channel3 : std_logic_vector(7 downto 0);

	signal sclk_cnt : unsigned(3 downto 0);
	signal adc_data : unsigned(7 downto 0);
	signal adc_data_out : unsigned(7 downto 0);

begin

	-- Instantiate the Unit Under Test (UUT)
	uut : entity axi_adc084s_v1_00_a.adc084s
		port map(clk      => clk,
			     rst      => rst,
			     clk_div  => clk_div,
			     en       => en,
			     done     => done,
			     cs_n     => cs_n,
			     sclk     => sclk,
			     mosi     => mosi,
			     miso     => adc_data_out(0),
			     channel0 => channel0,
			     channel1 => channel1,
			     channel2 => channel2,
			     channel3 => channel3);

	-- Clock process definitions
	clk_process : process
	begin
		clk <= '0';
		wait for CLOCK_PERIOD / 2;
		clk <= '1';
		wait for CLOCK_PERIOD / 2;
	end process;

	-- Stimulus process
	stim_proc : process
	begin
		rst     <= '1';
		en      <= '0';
		clk_div <= (others => '0');
		wait for RESET_TIME;
		rst     <= '0';
		clk_div <= std_logic_vector(to_unsigned(10, C_S_CLOCK_DIV_WIDTH));
		wait for 10 us;
		en <= '1';
		wait;
	end process;

	CLK_CNT : process(sclk)
	begin
		if cs_n = '0' and rst = '0' then
			if rising_edge(sclk) then
				sclk_cnt <= sclk_cnt + 1;
			end if;
		else
			sclk_cnt <= (others => '0');
		end if;
	end process;

	STIM_DATA : process(sclk, rst)
	begin
		if rst = '1' then
			adc_data_out <= (others => '0');
			adc_data  <= (others => '0');
		elsif falling_edge(sclk) then
			if sclk_cnt = 5 then
				adc_data <= adc_data + 1;
				adc_data_out <= adc_data;
			elsif sclk_cnt > 5 and  sclk_cnt < 13 then
				adc_data_out <= '0' & adc_data_out(7 downto 1);
			else
				adc_data_out  <= (others => 'Z');
			end if;
		end if;
	end process;
END;

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.ipif_pkg.all;

library interrupt_control_v2_01_a;
use interrupt_control_v2_01_a.interrupt_control;

library axi_adc084s_v1_00_a;
use axi_adc084s_v1_00_a.defs.all;
use axi_adc084s_v1_00_a.regs.all;
use axi_adc084s_v1_00_a.all;

entity adc084s_if is
	generic(
		--  -- AXI Parameters
		C_S_AXI_ADDR_WIDTH   : integer range 32 to 32  := 32;
		C_S_AXI_DATA_WIDTH   : integer range 32 to 128 := 32;
		C_IP_INTR_MODE_ARRAY : INTEGER_ARRAY_TYPE;
		C_NUM_CE_SIGNALS     : integer;
		C_NUM_CS_SIGNALS     : integer

	--Quad Decoder generics
	);
	port(
		Bus2IP_Clk   : in  std_logic;
		Bus2IP_Reset : in  std_logic;

		Bus2IP_BE    : in  std_logic_vector(0 to ((C_S_AXI_DATA_WIDTH / 8) - 1));
		Bus2IP_CS    : in  std_logic_vector(0 to (C_NUM_CS_SIGNALS - 1));
		Bus2IP_RdCE  : in  std_logic_vector(0 to (C_NUM_CE_SIGNALS - 1));
		Bus2IP_WrCE  : in  std_logic_vector(0 to (C_NUM_CE_SIGNALS - 1));
		Bus2IP_Data  : in  std_logic_vector(0 to (C_S_AXI_DATA_WIDTH - 1));
		Bus2IP_RNW   : in  std_logic;
		IP2Bus_Data  : out std_logic_vector(0 to (C_S_AXI_DATA_WIDTH - 1));
		IP2Bus_WrAck : out std_logic;
		IP2Bus_RdAck : out std_logic;

		IP2INTC_Irpt : out std_logic;

		cs_n         : out std_logic;
		sclk         : out std_logic;
		mosi         : out std_logic;
		miso         : in  std_logic;

		channel0_out : out std_logic_vector(7 downto 0);
		channel1_out : out std_logic_vector(7 downto 0);
		channel2_out : out std_logic_vector(7 downto 0);
		channel3_out : out std_logic_vector(7 downto 0);

		done_out     : out std_logic
	);
end entity adc084s_if;

architecture RTL of adc084s_if is

	-- Interface registers
	signal ip2Bus_IntrEvent_int : std_logic_vector(0 to (C_IP_INTR_MODE_ARRAY'length - 1));
	signal Bus2IP_Reset_i       : std_logic;
	signal Intr2Bus_DBus        : std_logic_vector(0 to C_S_AXI_DATA_WIDTH - 1);
	signal ADC084S2Bus_DBus     : std_logic_vector(0 to C_S_AXI_DATA_WIDTH - 1);

	signal channel0_i : std_logic_vector(7 downto 0);
	signal channel1_i : std_logic_vector(7 downto 0);
	signal channel2_i : std_logic_vector(7 downto 0);
	signal channel3_i : std_logic_vector(7 downto 0);

	signal clk_div : std_logic_vector(15 downto 0);
	signal en      : std_logic;
	signal done_i  : std_logic;

begin
	-------------------------------------------------------------------------
	-- Internal Buffers
	-------------------------------------------------------------------------	
	done_out       <= done_i;
	channel0_out   <= channel0_i;
	channel1_out   <= channel1_i;
	channel2_out   <= channel2_i;
	channel3_out   <= channel3_i;
	Bus2IP_Reset_i <= Bus2IP_Reset;

	SYNC_REG_READ : process(Bus2IP_Clk) is
	begin
		if rising_edge(Bus2IP_Clk) then
			if Bus2IP_Reset_i = '1' then
				ADC084S2Bus_DBus <= (others => '0');
			elsif or_reduce(Bus2IP_RdCE) = '1' then
				if Bus2IP_RdCE(C_CHANNEL0_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= std_logic_vector(resize(unsigned(channel0_i), C_S_AXI_DATA_WIDTH));
					report "read: channel 0:" & integer'image(to_integer(unsigned(channel0_i)));
				elsif Bus2IP_RdCE(C_CHANNEL1_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= std_logic_vector(resize(unsigned(channel1_i), C_S_AXI_DATA_WIDTH));
					report "read: channel 1:" & integer'image(to_integer(unsigned(channel1_i)));
				elsif Bus2IP_RdCE(C_CHANNEL2_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= std_logic_vector(resize(unsigned(channel2_i), C_S_AXI_DATA_WIDTH));
					report "read: channel 2:" & integer'image(to_integer(unsigned(channel2_i)));
				elsif Bus2IP_RdCE(C_CHANNEL3_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= std_logic_vector(resize(unsigned(channel3_i), C_S_AXI_DATA_WIDTH));
					report "read: channel 3:" & integer'image(to_integer(unsigned(channel3_i)));
				elsif Bus2IP_RdCE(C_STATUS_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= (0 => done_i, others => '0');
				elsif Bus2IP_RdCE(C_COMMAND_REG_OFFSET) = '1' then
					ADC084S2Bus_DBus <= (others => '0');
				end if;
			else
				ADC084S2Bus_DBus <= (others => '0');
			end if;

		end if;
	end process;

	SYNC_REG_WRITE : process(Bus2IP_Clk) is
	begin
		if rising_edge(Bus2IP_Clk) then
			if Bus2IP_Reset_i = '1' then
				clk_div <= (others => '0');
				en      <= '0';
			elsif or_reduce(Bus2IP_WrCE) = '1' then
				if Bus2IP_WrCE(C_COMMAND_REG_OFFSET) = '1' then
					clk_div <= Bus2IP_Data(15 downto 0);
					en      <= Bus2IP_Data(16);
				end if;
			end if;
		end if;
	end process SYNC_REG_WRITE;

	-------------------------------------------------------------------------------
	-- Read Data generation
	-------------------------------------------------------------------------------
	IP2Bus_Data <= ADC084S2Bus_DBus;

	ip2Bus_IntrEvent_int(C_INTR_DONE_BIT_OFFSET) <= done_i;

	INST_INTERRUPT_CONTROL_I : entity interrupt_control_v2_01_a.interrupt_control
		generic map(
			C_NUM_CE               => 16,
			C_NUM_IPIF_IRPT_SRC    => 1, -- Set to 1 to avoid null array
			C_IP_INTR_MODE_ARRAY   => C_IP_INTR_MODE_ARRAY,

			-- Specifies device Priority Encoder function
			C_INCLUDE_DEV_PENCODER => false,

			-- Specifies device ISC hierarchy
			C_INCLUDE_DEV_ISC      => false,
			C_IPIF_DWIDTH          => C_S_AXI_DATA_WIDTH
		)
		port map(
			Bus2IP_Clk          => Bus2IP_Clk,
			Bus2IP_Reset        => Bus2IP_Reset_i,
			Bus2IP_Data         => Bus2IP_Data,
			Bus2IP_BE           => Bus2IP_BE,
			Interrupt_RdCE      => Bus2IP_RdCE(C_INTR_CE_OFFSET to C_INTR_CE_OFFSET + C_INTR_NUM_USER_REGISTERS - 1),
			Interrupt_WrCE      => Bus2IP_WrCE(C_INTR_CE_OFFSET to C_INTR_CE_OFFSET + C_INTR_NUM_USER_REGISTERS - 1),
			IPIF_Reg_Interrupts => "00", -- Tie off the unused reg intrs
			IPIF_Lvl_Interrupts => "0", -- Tie off the dummy lvl intr
			IP2Bus_IntrEvent    => ip2Bus_IntrEvent_int,
			Intr2Bus_DevIntr    => IP2INTC_Irpt,
			Intr2Bus_DBus       => Intr2Bus_DBus,
			Intr2Bus_WrAck      => open,
			Intr2Bus_RdAck      => open,
			Intr2Bus_Error      => open,
			Intr2Bus_Retry      => open,
			Intr2Bus_ToutSup    => open
		);

	-------------------------------------------------------------------------------
	-- SYNC_ACK_GEN : Ack generation
	-------------------------------------------------------------------------------
	SYNC_ACK_GEN : process(Bus2IP_Clk)
	begin
		if rising_edge(Bus2IP_Clk) then
			IP2Bus_RdAck <= or_reduce(Bus2IP_CS) and Bus2IP_RNW;
			IP2Bus_WrAck <= or_reduce(Bus2IP_CS) and not Bus2IP_RNW;
		end if;
	end process;

	-------------------------------------------------------------------------------
	-- INST_ADC084S: adc084s Instance
	-------------------------------------------------------------------------------
	INST_ADC084S : entity axi_adc084s_v1_00_a.adc084s
		port map(clk      => Bus2IP_Clk,
			     rst      => Bus2IP_Reset_i,
			     clk_div  => clk_div,
			     en       => en,
			     done     => done_i,
			     cs_n     => cs_n,
			     sclk     => sclk,
			     mosi     => mosi,
			     miso     => miso,
			     channel0 => channel0_i,
			     channel1 => channel1_i,
			     channel2 => channel2_i,
			     channel3 => channel3_i);

end architecture RTL;

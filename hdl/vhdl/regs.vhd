library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

package regs is

	-------------------------------------------------------------------------
	-- 							ADDRESS BLOCKS
	-------------------------------------------------------------------------	
	constant INTR_BLOCK_BASEADDR : std_logic_vector(63 downto 0) := X"0000000000000000";
	constant INTR_BLOCK_HIGHADDR : std_logic_vector(63 downto 0) := X"000000000000007F";
	constant DATA_BLOCK_BASEADDR : std_logic_vector(63 downto 0) := X"0000000000000080";
	constant DATA_BLOCK_HIGHADDR : std_logic_vector(63 downto 0) := X"00000000000000FF";

	-------------------------------------------------------------------------
	-- 							REGISTER MAPS 
	-------------------------------------------------------------------------	

	-------------------------------------------------------------------------
	-- INTR : Interrupt Registers
	-------------------------------------------------------------------------		
	constant C_INTR_CE_OFFSET          : natural := 0;
	constant C_INTR_NUM_USER_REGISTERS : natural := 16;

	-------------------------------------------------------------------------
	-- SETUP : Setup Registers
	-------------------------------------------------------------------------
	constant C_DATA_CE_OFFSET : natural := C_INTR_CE_OFFSET + C_INTR_NUM_USER_REGISTERS;

	-- Setup memory map offsets [R/W]
	constant C_COMMAND_REG_OFFSET      : natural := 0 + C_DATA_CE_OFFSET; --4   
	constant C_STATUS_REG_OFFSET       : natural := 1 + C_DATA_CE_OFFSET; --C   
	constant C_CHANNEL0_REG_OFFSET     : natural := 2 + C_DATA_CE_OFFSET; --C   
	constant C_CHANNEL1_REG_OFFSET     : natural := 3 + C_DATA_CE_OFFSET; --C   
	constant C_CHANNEL2_REG_OFFSET     : natural := 4 + C_DATA_CE_OFFSET; --C   
	constant C_CHANNEL3_REG_OFFSET     : natural := 5 + C_DATA_CE_OFFSET; --C   
	constant C_DATA_NUM_USER_REGISTERS : natural := 6; -- (Must be last. Not an actual register)


	-------------------------------------------------------------------------
	-- 								BIT MAPS 
	-------------------------------------------------------------------------

	-------------------------------------------------------------------------
	-- INTR_DGIER_ADDR_OFFSET : Bit map
	-- INTR_IPISR_ADDR_OFFSET : Bit map
	-- INTR_IPIER_ADDR_OFFSET : Bit map
	-------------------------------------------------------------------------
	constant C_INTR_DONE_BIT_OFFSET : natural := 0;
	constant C_INTR_BIT_WIDTH       : natural := 1; -- (Must be last. Not an actual bit)


end package regs;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.ipif_pkg.all;

library axi_lite_ipif_v1_01_a;
use axi_lite_ipif_v1_01_a.axi_lite_ipif;

library axi_adc084s_v1_00_a;
use axi_adc084s_v1_00_a.defs.all;
use axi_adc084s_v1_00_a.regs.all;
use axi_adc084s_v1_00_a.all;

-------------------------------------------------------------------------------
--                     Definition of Generics
-------------------------------------------------------------------------------

--------------------
-- AXI LITE Generics
--------------------
-- C_S_AXI_DATA_WIDTH    -- AXI data bus width
-- C_S_AXI_ADDR_WIDTH    -- AXI address bus width
-- C_S_AXI_MIN_SIZE      -- Minimum address range of the IP
-- C_USE_WSTRB           -- Use write strobs or not
-- C_DPHASE_TIMEOUT      -- Data phase time out counter
-- C_ARD_ADDR_RANGE_ARRAY-- Base /High Address Pair for each Address Range
-- C_ARD_NUM_CE_ARRAY    -- Desired number of chip enables for an address range
-- C_FAMILY              -- Target FPGA family

-------------------------------------------------------------------------------
--                  Definition of Ports
-------------------------------------------------------------------------------
-- S_AXI_ACLK            -- AXI Clock
-- S_AXI_ARESETN         -- AXI Reset
-- S_AXI_AWADDR          -- AXI Write address
-- S_AXI_AWVALID         -- Write address valid
-- S_AXI_AWREADY         -- Write address ready
-- S_AXI_WDATA           -- Write data
-- S_AXI_WSTRB           -- Write strobes
-- S_AXI_WVALID          -- Write valid
-- S_AXI_WREADY          -- Write ready
-- S_AXI_BRESP           -- Write response
-- S_AXI_BVALID          -- Write response valid
-- S_AXI_BREADY          -- Response ready
-- S_AXI_ARADDR          -- Read address
-- S_AXI_ARVALID         -- Read address valid
-- S_AXI_ARREADY         -- Read address ready
-- S_AXI_RDATA           -- Read data
-- S_AXI_RRESP           -- Read response
-- S_AXI_RVALID          -- Read valid
-- S_AXI_RREADY          -- Read ready

-------------------------------------------------------------------------------
-- Entity Declaration
-------------------------------------------------------------------------------

entity axi_adc084s is
	generic(
		--  -- System Parameter
		C_FAMILY           : string                        := "spartan6";
		C_INSTANCE         : string                        := "axi_adc084s_inst";
		--  -- AXI Parameters
		C_BASEADDR         : std_logic_vector(31 downto 0) := X"FFFF_FFFF";
		C_HIGHADDR         : std_logic_vector(31 downto 0) := X"0000_0000";
		C_S_AXI_ADDR_WIDTH : integer range 32 to 32        := 32;
		C_S_AXI_DATA_WIDTH : integer range 32 to 128       := 32

	-- ADC084S generics 
	);
	port(
		--System signals
		S_AXI_ACLK    : in  std_logic;
		S_AXI_ARESETN : in  std_logic;
		-- AXI Write address channel signals
		S_AXI_AWADDR  : in  std_logic_vector((C_S_AXI_ADDR_WIDTH - 1) downto 0);
		S_AXI_AWVALID : in  std_logic;
		S_AXI_AWREADY : out std_logic;
		-- AXI Write data channel signals
		S_AXI_WDATA   : in  std_logic_vector((C_S_AXI_DATA_WIDTH - 1) downto 0);
		S_AXI_WSTRB   : in  std_logic_vector(((C_S_AXI_DATA_WIDTH / 8) - 1) downto 0);
		S_AXI_WVALID  : in  std_logic;
		S_AXI_WREADY  : out std_logic;
		-- AXI Write response channel signals
		S_AXI_BRESP   : out std_logic_vector(1 downto 0);
		S_AXI_BVALID  : out std_logic;
		S_AXI_BREADY  : in  std_logic;
		-- AXI Read address channel signals
		S_AXI_ARADDR  : in  std_logic_vector((C_S_AXI_ADDR_WIDTH - 1) downto 0);
		S_AXI_ARVALID : in  std_logic;
		S_AXI_ARREADY : out std_logic;
		-- AXI Read address channel signals
		S_AXI_RDATA   : out std_logic_vector((C_S_AXI_DATA_WIDTH - 1) downto 0);
		S_AXI_RRESP   : out std_logic_vector(1 downto 0);
		S_AXI_RVALID  : out std_logic;
		S_AXI_RREADY  : in  std_logic;

		cs_n          : out std_logic;
		sclk          : out std_logic;
		mosi          : out std_logic;
		miso          : in  std_logic;

		channel0_out  : out std_logic_vector(7 downto 0);
		channel1_out  : out std_logic_vector(7 downto 0);
		channel2_out  : out std_logic_vector(7 downto 0);
		channel3_out  : out std_logic_vector(7 downto 0);

		done_out      : out  std_logic
	);

	-------------------------------------------------------------------------------
	-- Fan-Out attributes for XST
	-------------------------------------------------------------------------------

	ATTRIBUTE MAX_FANOUT : string;
	ATTRIBUTE MAX_FANOUT of S_AXI_ACLK : signal is "10000";
	ATTRIBUTE MAX_FANOUT of S_AXI_ARESETN : signal is "10000";

	-----------------------------------------------------------------
	-- Start of PSFUtil MPD attributes
	-----------------------------------------------------------------

	ATTRIBUTE ADDR_TYPE : string;
	ATTRIBUTE ASSIGNMENT : string;
	ATTRIBUTE HDL : string;
	ATTRIBUTE IMP_NETLIST : string;
	ATTRIBUTE IPTYPE : string;
	ATTRIBUTE MIN_SIZE : string;
	ATTRIBUTE SIGIS : string;
	ATTRIBUTE STYLE : string;

	ATTRIBUTE ADDR_TYPE of C_BASEADDR : constant is "REGISTER";
	ATTRIBUTE ADDR_TYPE of C_HIGHADDR : constant is "REGISTER";
	ATTRIBUTE ASSIGNMENT of C_HIGHADDR : constant is "REQUIRE";
	ATTRIBUTE HDL of axi_adc084s : entity is "VHDL";
	ATTRIBUTE IMP_NETLIST of axi_adc084s : entity is "TRUE";
	ATTRIBUTE IPTYPE of axi_adc084s : entity is "PERIPHERAL";

	ATTRIBUTE SIGIS of S_AXI_ACLK : signal is "CLK";
	ATTRIBUTE SIGIS of S_AXI_ARESETN : signal is "RST";
	ATTRIBUTE STYLE of axi_adc084s : entity is "HDL";

end entity axi_adc084s;
-------------------------------------------------------------------------------
-- Architecture Section
-------------------------------------------------------------------------------
architecture imp of axi_adc084s is

	------------------------------
	-- Constant declarations
	------------------------------
	-- AXI lite parameters
	constant C_S_AXI_ADC084S_MIN_SIZE : std_logic_vector(31 downto 0) := X"00000200";
	constant C_USE_WSTRB              : integer                       := 1;
	constant C_DPHASE_TIMEOUT         : integer                       := 8;

	constant C_IP_INTR_MODE_ARRAY : INTEGER_ARRAY_TYPE(0 to (C_INTR_BIT_WIDTH - 1)) := (0 => INTR_POS_EDGE_DETECT);

	constant C_ARD_ADDR_RANGE_ARRAY : SLV64_ARRAY_TYPE := (
		INTR_BLOCK_BASEADDR,            -- Interrupt controller
		INTR_BLOCK_HIGHADDR,
		DATA_BLOCK_BASEADDR,            -- Data registers (FIFO)
		DATA_BLOCK_HIGHADDR
	);

	constant C_ARD_NUM_CE_ARRAY : INTEGER_ARRAY_TYPE := (
		0 => C_INTR_NUM_USER_REGISTERS,
		1 => C_DATA_NUM_USER_REGISTERS
	);

	constant C_NUM_CE_SIGNALS : integer := calc_num_ce(C_ARD_NUM_CE_ARRAY);
	constant C_NUM_CS_SIGNALS : integer := (C_ARD_ADDR_RANGE_ARRAY'LENGTH / 2);

	-------------------------------------------------------------------------------
	signal bus2ip_clk       : std_logic;
	signal bus2ip_be_int    : std_logic_vector(((C_S_AXI_DATA_WIDTH / 8) - 1) downto 0);
	signal bus2ip_cs_int    : std_logic_vector((C_NUM_CS_SIGNALS - 1) downto 0);
	signal bus2ip_rdce_int  : std_logic_vector((C_NUM_CE_SIGNALS - 1) downto 0);
	signal bus2ip_wrce_int  : std_logic_vector((C_NUM_CE_SIGNALS - 1) downto 0);
	signal bus2ip_rnw       : std_logic                                           := '0';
	signal bus2ip_data_int  : std_logic_vector((C_S_AXI_DATA_WIDTH - 1) downto 0);
	signal ip2bus_data_int  : std_logic_vector((C_S_AXI_DATA_WIDTH - 1) downto 0) := (others => '0');
	signal ip2bus_wrack_int : std_logic                                           := '0';
	signal ip2bus_rdack_int : std_logic                                           := '0';

	signal bus2ip_reset_int : std_logic;

	signal bus2ip_reset_int_core : std_logic;

begin
	--------------------------------------------------------------------------
	-- Instantiate AXI lite IPIF
	--------------------------------------------------------------------------
	AXI_LITE_IPIF_I : entity axi_lite_ipif_v1_01_a.axi_lite_ipif
		generic map(
			C_S_AXI_ADDR_WIDTH     => C_S_AXI_ADDR_WIDTH,
			C_S_AXI_DATA_WIDTH     => C_S_AXI_DATA_WIDTH,
			C_S_AXI_MIN_SIZE       => C_S_AXI_ADC084S_MIN_SIZE,
			C_USE_WSTRB            => C_USE_WSTRB,
			C_DPHASE_TIMEOUT       => C_DPHASE_TIMEOUT,
			C_ARD_ADDR_RANGE_ARRAY => C_ARD_ADDR_RANGE_ARRAY,
			C_ARD_NUM_CE_ARRAY     => C_ARD_NUM_CE_ARRAY,
			C_FAMILY               => C_FAMILY
		)
		port map(
			S_AXI_ACLK    => S_AXI_ACLK, -- in
			S_AXI_ARESETN => S_AXI_ARESETN, -- in

			S_AXI_AWADDR  => S_AXI_AWADDR, -- in
			S_AXI_AWVALID => S_AXI_AWVALID, -- in
			S_AXI_AWREADY => S_AXI_AWREADY, -- out
			S_AXI_WDATA   => S_AXI_WDATA, -- in
			S_AXI_WSTRB   => S_AXI_WSTRB, -- in
			S_AXI_WVALID  => S_AXI_WVALID, -- in
			S_AXI_WREADY  => S_AXI_WREADY, -- out
			S_AXI_BRESP   => S_AXI_BRESP, -- out
			S_AXI_BVALID  => S_AXI_BVALID, -- out
			S_AXI_BREADY  => S_AXI_BREADY, -- in
			S_AXI_ARADDR  => S_AXI_ARADDR, -- in
			S_AXI_ARVALID => S_AXI_ARVALID, -- in
			S_AXI_ARREADY => S_AXI_ARREADY, -- out
			S_AXI_RDATA   => S_AXI_RDATA, -- out
			S_AXI_RRESP   => S_AXI_RRESP, -- out
			S_AXI_RVALID  => S_AXI_RVALID, -- out
			S_AXI_RREADY  => S_AXI_RREADY, -- in

			-- IP Interconnect (IPIC) port signals
			Bus2IP_Clk    => bus2ip_clk, -- out
			Bus2IP_Resetn => bus2ip_reset_int, -- out

			Bus2IP_Addr   => open,      -- bus2ip_addr_int,  -- out
			Bus2IP_RNW    => bus2ip_rnw, -- out
			Bus2IP_BE     => bus2ip_be_int, -- out
			Bus2IP_CS     => bus2ip_cs_int, -- out
			Bus2IP_RdCE   => bus2ip_rdce_int, -- out
			Bus2IP_WrCE   => bus2ip_wrce_int, -- out
			Bus2IP_Data   => bus2ip_data_int, -- out

			IP2Bus_Data   => ip2bus_data_int, -- in
			IP2Bus_WrAck  => ip2bus_wrack_int, -- in
			IP2Bus_RdAck  => ip2bus_rdack_int, -- in
			IP2Bus_Error  => '0'        -- in
		);

	--------------------------------------------------------------------------
	-- SYNC_RST_INV: convert active low to active hig reset to rest of the core.
	--------------------------------------------------------------------------
	SYNC_RST_INV : process(S_AXI_ACLK) is
	begin
		if rising_edge(S_AXI_ACLK) then
			bus2ip_reset_int_core <= not (bus2ip_reset_int);
		end if;
	end process SYNC_RST_INV;

	--------------------------------------------------------------------------
	-- Instansiating the Quad Decoder IF
	--------------------------------------------------------------------------
	INST_QUAD_DECODER_IF : entity axi_adc084s_v1_00_a.adc084s_if
		generic map(C_S_AXI_ADDR_WIDTH   => C_S_AXI_ADDR_WIDTH,
			        C_S_AXI_DATA_WIDTH   => C_S_AXI_DATA_WIDTH,
			        C_NUM_CE_SIGNALS     => C_NUM_CE_SIGNALS,
			        C_NUM_CS_SIGNALS     => C_NUM_CS_SIGNALS,
			        C_IP_INTR_MODE_ARRAY => C_IP_INTR_MODE_ARRAY
		)
		port map(Bus2IP_Clk   => bus2ip_clk,
			     Bus2IP_Reset => bus2ip_reset_int_core,
			     Bus2IP_BE    => bus2ip_be_int,
			     Bus2IP_CS    => bus2ip_cs_int,
			     Bus2IP_RdCE  => bus2ip_rdce_int,
			     Bus2IP_WrCE  => bus2ip_wrce_int,
			     Bus2IP_Data  => bus2ip_data_int,
			     Bus2IP_RNW   => bus2ip_rnw,
			     IP2Bus_Data  => ip2bus_data_int,
			     IP2Bus_WrAck => ip2bus_wrack_int,
			     IP2Bus_RdAck => ip2bus_rdack_int,
			     cs_n         => cs_n,
			     sclk         => sclk,
			     mosi         => mosi,
			     miso         => miso,
			     channel0_out => channel0_out,
			     channel1_out => channel1_out,
			     channel2_out => channel2_out,
			     channel3_out => channel3_out,
			     done_out     => done_out
		);

end architecture imp;

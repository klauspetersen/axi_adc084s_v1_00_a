use STD.textio.all;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

library axi_adc084s_v1_00_a;
use axi_adc084s_v1_00_a.defs.all;
use axi_adc084s_v1_00_a.regs.all;
use axi_adc084s_v1_00_a.all;

entity adc084s is
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;       -- Active High Sync. reset
		clk_div  : in  std_logic_vector(C_S_CLOCK_DIV_WIDTH - 1 downto 0);
		en       : in  std_logic;
		done     : out std_logic;
		cs_n     : out std_logic;
		sclk     : out std_logic;
		mosi     : out std_logic;
		miso     : in  std_logic;
		channel0 : out std_logic_vector(7 downto 0);
		channel1 : out std_logic_vector(7 downto 0);
		channel2 : out std_logic_vector(7 downto 0);
		channel3 : out std_logic_vector(7 downto 0)
	);
end entity adc084s;

architecture RTL of adc084s is
	signal spi_clk_pos_strobe : std_logic;
	signal spi_clk_neg_strobe : std_logic;
	signal spi_clk_i          : std_logic;
	signal spi_divclk_cnt     : unsigned(C_S_CLOCK_DIV_WIDTH - 1 downto 0);
	signal clk_en             : std_logic;
	signal cnt_en             : std_logic;
	signal cnt_clr            : std_logic;
	signal spi_clk_cnt        : unsigned(3 downto 0);
	signal data_valid         : std_logic;
	signal channel_sel        : unsigned(1 downto 0);
	signal cmd_sel            : unsigned(1 downto 0);
	signal cmd_en             : std_logic;
	signal cmd_load           : std_logic;
	signal data_out           : std_logic_vector(7 downto 0);
	signal data_in            : std_logic_vector(7 downto 0);

	type s_states is (s_disabled, s_init, s_io);
	signal s_state : s_states;

	type spi_cmds_ar is array (0 to C_NUM_SPI_CMDS - 1) of std_logic_vector(7 downto 0);

	signal spi_cmds : spi_cmds_ar := (
		0 => "11111111",
		1 => "11111111",
		2 => "11111111",
		3 => "11111111"
	--		0 => "00000001",
	--		1 => "00000011",
	--		2 => "00000111",
	--		3 => "00001111"
	);

begin

	-------------------------------------------------------------------------
	-- Internal Buffers
	-------------------------------------------------------------------------	
	sclk <= spi_clk_i;
	mosi <= data_out(7);

	-------------------------------------------------------------------------
	-- SYNC_SPI_CLK : Spi Clock generator
	-------------------------------------------------------------------------
	SYNC_SPI_CLK : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				spi_divclk_cnt     <= (others => '0');
				spi_clk_i          <= '0';
				spi_clk_pos_strobe <= '0';
				spi_clk_neg_strobe <= '0';
			else
				if clk_en = '1' then
					if spi_divclk_cnt = unsigned(clk_div) then
						spi_divclk_cnt <= (others => '0');
						spi_clk_i      <= not spi_clk_i;

						if spi_clk_i = '0' then
							spi_clk_pos_strobe <= '1';
						else
							spi_clk_neg_strobe <= '1';
						end if;
					else
						spi_divclk_cnt     <= spi_divclk_cnt + 1;
						spi_clk_pos_strobe <= '0';
						spi_clk_neg_strobe <= '0';
					end if;
				else
					spi_divclk_cnt     <= (others => '0');
					spi_clk_i          <= '0';
					spi_clk_pos_strobe <= '0';
					spi_clk_neg_strobe <= '0';
				end if;
			end if;
		end if;
	end process;

	-------------------------------------------------------------------------
	-- SYNC_CLK_CNT : Spi Clock Counter
	-------------------------------------------------------------------------
	SYNC_CLK_CNT : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				spi_clk_cnt <= (others => '0');
			else
				if cnt_clr = '1' then
					spi_clk_cnt <= (others => '0');
				elsif cnt_en = '1' then
					if spi_clk_pos_strobe = '1' then
						spi_clk_cnt <= spi_clk_cnt + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	-------------------------------------------------------------------------
	-- SYNC_DATA_SR : Data output shift register
	-------------------------------------------------------------------------
	SYNC_DATA_OUT_SR : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				data_out <= (others => '0');
			else
				if cmd_load = '1' then
					data_out <= spi_cmds(to_integer(unsigned(cmd_sel)));
				elsif cmd_en = '1' then
					if spi_clk_neg_strobe = '1' then
						data_out <= data_out(6 downto 0) & '0';
					end if;
				end if;
			end if;
		end if;
	end process;

	-------------------------------------------------------------------------
	-- SYNC_DESERIALIZE : Data input deserializer
	-------------------------------------------------------------------------
	SYNC_DESERIALIZE : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				data_in <= (others => '0');
			elsif spi_clk_pos_strobe = '1' then
				data_in <= miso & data_in(7 downto 1);
			end if;
		end if;
	end process;

	-------------------------------------------------------------------------
	-- SYNC_DATA_STORE : Data storage
	-------------------------------------------------------------------------
	SYNC_DATA : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				channel0 <= (others => '0');
				channel1 <= (others => '0');
				channel2 <= (others => '0');
				channel3 <= (others => '0');
				channel_sel <= (others => '0');
			elsif data_valid = '1' then
				channel_sel <= channel_sel + 1;
				case channel_sel is
					when "00" => 
						channel0 <= data_in;
					when "01" => 
						channel1 <= data_in;
					when "10" => 
						channel2 <= data_in;
					when "11" =>
						channel3 <= data_in;
						done  <= '1';
					when others =>
						null;
				end case;
			else
				done  <= '0';
			end if;
		end if;
	end process;

	-------------------------------------------------------------------------
	-- SYNC_FSM : State machine
	-------------------------------------------------------------------------	
	SYNC_FSM : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				s_state <= s_disabled;
				cmd_sel <= (others => '0');
			else
				case s_state is
					when s_disabled =>
						clk_en      <= '0';
						cnt_en      <= '0';
						cnt_clr     <= '0';
						cs_n        <= '1';
						cmd_sel     <= "00";
						cmd_load    <= '0';
						cmd_en      <= '0';
						data_valid  <= '0';

						if en = '1' then
							cnt_clr <= '1';
							s_state <= s_init;
						end if;

					when s_init =>
						clk_en      <= '1';
						cnt_en      <= '0';
						cnt_clr     <= '0';
						cs_n        <= '0';
						cmd_en      <= '1';
						data_valid  <= '0';

						if en = '0' then
							s_state <= s_disabled;
						elsif spi_clk_neg_strobe = '1' then
							s_state  <= s_io;
							cmd_load <= '1';
							cmd_sel  <= cmd_sel + 1;
						end if;

					when s_io =>
						clk_en      <= '1';
						cnt_en      <= '1';
						cnt_clr     <= '0';
						cmd_en      <= '1';
						cmd_load    <= '0';

						if en = '0' then
							s_state <= s_disabled;
						elsif spi_clk_cnt = to_unsigned(0, spi_clk_cnt'length) and spi_clk_neg_strobe = '1' then
							cmd_load <= '1';
							cmd_sel  <= cmd_sel + 1;
						end if;


						if spi_clk_cnt = to_unsigned(12, spi_clk_cnt'length) and spi_clk_neg_strobe = '1' then
							data_valid <= '1';
						else 
							data_valid <= '0';
						end if;
				end case;
			end if;
		end if;
	end process SYNC_FSM;

end architecture RTL;

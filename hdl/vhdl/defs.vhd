library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

package defs is
	constant CLOCK_FREQ : real := 100000000.0;

	constant CLOCK_PERIOD : time := (1.0 sec / CLOCK_FREQ);

	constant RESET_TIME : time := 20 us;

	constant MIN_RESET_CLOCK_CYCLES : integer := 10;

	constant SAMPLE_TIME_OFFSET : time := RESET_TIME;
	
	constant C_S_BUS_WIDTH : integer  := 32;
	
	constant C_S_CLOCK_DIV_WIDTH : integer  := 16;
	
	constant C_NUM_SPI_CMDS : integer  := 4;
	
end package defs;

package body defs is

end package body defs;

